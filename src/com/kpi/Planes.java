package com.kpi;

/**
 * Created by Yuriy on 08.05.2016.
 */
public class Planes {
    private String PlaneName;
    private int RangeofFlight;
    private double FuelConsumtion;
    private int PlaneCapacity;

    //Setters
    public void SetPlaneName(String name){
        this.PlaneName = name;
    }
    public void SetRange(int range){
        this.RangeofFlight = range;
    }
    public void SetConsumtion(double consume){
        this.FuelConsumtion = consume;
    }
    public void SetCapacity(int capac){
        this.PlaneCapacity = capac;
    }

    //Constructors
    public Planes(){
        SetPlaneName("ИЛ");
        SetRange(4000);
        SetConsumtion(300.6);
        SetCapacity(1200);
    }
    public Planes(int r, double f, int c){
        SetRange(r);
        SetConsumtion(f);
        SetCapacity(c);
        SetPlaneName("ИЛ");
    }

    //Getters
    String GetPlaneName(){
        return PlaneName;
    }
    int GetRange(){
        return RangeofFlight;
    }
    double GetConsumption(){
        return FuelConsumtion;
    }
    int GetCapacity(){
        return PlaneCapacity;
    }
}
