package com.kpi;

/**
 * Created by Yuriy on 08.05.2016.
 */
public class Boeing extends Planes {
    public Boeing(){
        super();
        SetPlaneName("Boeing");
    }
    public Boeing(int r, double f, int c){
        super(r,f,c);
        SetPlaneName("Boeing");
    }
}
